require_relative '../lib/api_interactions.rb'

describe 'API tests for GIPHY search API' do

  it "Searches for term with an undefined limit and offset", :get do
    # Search for a given term
    api = searchTerm(query: 'pasadena')
    # Assertions
    expect(api.status).to eq(200)
    expect(api.body['data'].length).to eq(50) # Default limit is 50
    expect(api.body['pagination']['count']).to eq(50) # Default limit is 50
    expect(api.body['pagination']['offset']).to eq(0)
    expect(api.body['meta']['msg']).to eq('OK')
  end

  it "Searches for term with zero offset", :get do
    # Search for a given term
    api = searchTerm(query: 'pasadena', limit: '25', offset: '0')
    # Assertions
    expect(api.status).to eq(200)
    expect(api.body['data'].length).to eq(25)
    expect(api.body['pagination']['count']).to eq(25)
    expect(api.body['pagination']['offset']).to eq(0)
    expect(api.body['meta']['msg']).to eq('OK')
  end

  it "Searches for term using an offset greater than zero", :get do
    # Search for a given term
    api = searchTerm(query: 'funny', limit: '5', offset: '5')
    # Assertions
    expect(api.status).to eq(200)
    expect(api.body['pagination']['count']).to eq(5)
    expect(api.body['pagination']['offset']).to eq(5)
    expect(api.body['meta']['msg']).to eq('OK')
    expect(api.body['data'].length).to eq(5)
  end

  it "Searches for term using a term without results", :get do
    # Search for a given term
    api = searchTerm(query: 'Antony_Fuentes_Costa_Rica_Doesnt_Exist', limit: '5', offset: '5')
    # Assertions
    expect(api.status).to eq(200)
    expect(api.body['data']).to be_empty
    expect(api.body['pagination']['total_count']).to eq(0)
    expect(api.body['pagination']['count']).to eq(0)
    expect(api.body['pagination']['offset']).to eq(5)
    expect(api.body['meta']['msg']).to eq('OK')    
  end

  it "Searches for term with invalid offset", :get do
    # Search for a given term
    api = searchTerm(query: 'pasadena', limit: '25', offset: '500000000')
    # Assertions
    expect(api.status).to eq(200)
    expect(api.body['data']).to be_empty
    expect(api.body['pagination']['total_count']).to eq(0)
    expect(api.body['pagination']['count']).to eq(0)
    expect(api.body['pagination']['offset']).to eq(0)
    expect(api.body['meta']['msg']).to eq('OK')    
  end

  it "Searches using an empty API_KEY", :get do
    # Search for a given term
    api = searchTerm(query: 'funny', limit: '5', offset: '5', api_key: '')
    # Assertions
    expect(api.status).to eq(401)
    expect(api.body['data']).to be_empty
    expect(api.body['pagination']).to be_nil
    expect(api.body['meta']['msg']).to eq('No API key found in request.')
  end

  it "Searches using an invalid API_KEY", :get do
    # Search for a given term
    api = searchTerm(query: 'funny', limit: '5', offset: '5', api_key: 'invalid_api_key')
    # Assertions
    expect(api.status).to eq(401)
    expect(api.body['data']).to be_empty
    expect(api.body['pagination']).to be_nil
    expect(api.body['meta']['msg']).to eq('Unauthorized')
  end

end
