require "bundler/setup"
require "client-api"
require "rspec"
require "rspec/expectations"
require "json"

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end

ClientApi.configure do |config|
  config.before(:each) do
    @api_key = ENV['API_KEY']
  end

  config.base_url = 'https://api.giphy.com/v1/gifs'
  config.headers = {'Content-Type' => 'application/json', 'Accept' => 'application/json'}
  config.json_output = {'Dirname' => './output', 'Filename' => 'test'}
  config.time_out = 10  # in secs

  # Logging
  config.logger = {'Dirname' => './logs', 'Filename' => 'test', 'StoreFilesCount' => 2}
  config.before(:each) do |scenario|
    ClientApi::Request.new(scenario)
  end
end
