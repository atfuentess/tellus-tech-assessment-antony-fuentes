def searchTerm(params = {}) 
  api = ClientApi::Api.new
  api.get({
    url: '/search',
    query: {
        'q': params.fetch(:query, ''),
        'api_key': params.fetch(:api_key, @api_key),
        'limit': params.fetch(:limit, ''),
        'offset': params.fetch(:offset, '')
    }
  })
  return api
end
