# Test assignment for the QA Engineer in Automation (Backend focus)

## I. Back-end test

Please use Ruby.
Results in Git, please.
Implement Search Test for GIPHY WEB app: https://giphy.com/

Test Description:
1. Send search request; specify query; offset and limit for pagination
2. Verify response is correct

**Result:** [first_challenge/](first_challenge/)

## II. SQL test

Given you have table of products with fields id, product_name, product_type, product_model, price in database,

And product_type can be desktop or laptop,
And model can be any String,
And name can be any String,

Select all product names for laptops from table products with average price < $2000

**Result:** [second_challenge/README.md](second_challenge/README.md)

## III. Test Scenarios
1. Cover Search functionality for GIPHY WEB app (https://giphy.com/), with high-level test scenarios, without detailed description.
2. Provide detailed description for one of the test scenarios, with test steps and expected result.

**Result:** [third_challenge/README.md](third_challenge/README.md)

## Thanks!!!

Thank you for giving me the chance to show you my skills. I hope you like my work. I'm looking forward to hearing from you.
