# III. Test Scenarios

Test coverage for GIPHY WEB app

## Detailed test, with test steps and expected result

![Detailed test case screenshot](detailed-test.png)

## High-level test scenarios, without detailed description

**Navigation bar**
- Validate the top navigation bar is displayed
- Validate that clicking on the logo redirects to the home page
- Validate that the navigation link for "Reactions" navigates to the correct page
- Validate that the navigation link for "Entertainment" navigates to the correct page
- Validate that the navigation link for "Sports" navigates to the correct page
- Validate that the navigation link for "Stickers" navigates to the correct page
- Validate that the navigation link for "Artists" navigates to the correct page
- Validate that the navigation link for "Upload" navigates to the correct page
- Validate that the navigation link for "Create" navigates to the correct page
- Validate that the navigation link for "Login" navigates to the correct page

**Login**
- Validate that the login page is displayed
- Validate that users can login with valid credentials
- Validate that users cannot login with invalid credentials
- Validate that users can logout
- Validate that users can login with Facebook
- Validate that users can login with Apple
- Validate that users can reset their password
- Validate that users can sign up

**Search**
- Validate that the search bar is displayed
- Validate that users can search for GIFs by typing a term and pressing enter
- Validate that users can search for GIFs by typing a term and clicking the search button
- Validate that the search input shows suggestions as the user types
- Validate that search suggestioons can be selected by clicking on them
- Validate that empty search results are displayed when no results are found
- Validate that searching without a term displays the home page

**Home page**
- ***Trending carousel***
  - Validate that the trending GIFs are displayed
  - Validate that trending GIFs carousel can be scrolled horizontally
  - Validate that clicking on a trending GIF redirects to the correct page
  - Validate that clicking on the "All The GIFs" button redirects to the correct page
  - Validate that GIFs can be added to the favorites list by clicking on the heart icon
  - Validate that GIFs can be removed from the favorites list by clicking on the heart icon
  - Validate that GIFs link can be copied by clicking on the "Copy Link" icon
- ***Artists carousel***
  - Validate that the artists carousel is displayed
  - Validate that artists carousel can be scrolled horizontally
  - Validate that clicking on an artist redirects to the correct page
  - Validate that clicking on the "All GIPHY Artists" button redirects to the correct page
  - Validate that each artist card displays the artist name
  - Validate that each artist card displays the artist avatar
  - Validate that each artist card displays the artist work availability
- ***Clips***
  - Validate that the clips section is displayed
  - Validate that clips show a title
  - Validate that clips show an attribution user name
  - Validate that clicking on the "All Clips" button redirects to the correct page
  - Validate that clicking on a clip redirects to the correct page
  - Validate that hovering a video plays the video and sound automatically
- ***Stories***
  - Validate that the stories section is displayed
  - Validate that stories show an avatar related to its category
  - Validate that stories show a title
  - Validate that stories show a date of when it was published
  - Validate that clicking on a story redirects to the correct page
- ***Footer***
  - Validate that "Privacy" link redirects to the correct page
  - Validate that "Terms" link redirects to the correct page

**Lazyloading and pagination**
- Validate that the GIFs are loaded lazily
- Validate that the GIFs are paginated when scrolling down the page
