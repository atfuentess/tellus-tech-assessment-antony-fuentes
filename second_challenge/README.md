# II. SQL test

## Given a table called `products` with the fields:
- id
- product_name `(Can be any String)`
- product_type `(Can be either "desktop" or "laptop")`
- product_model `(Can be any String)`
- price

## Select all product names for laptops from table products with average price < $2000. 

**Solution:**

```sql
SELECT product_name
FROM products
WHERE product_type = 'laptop' AND price < 2000;
```
